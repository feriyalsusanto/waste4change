import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waste4change/client/dio_client.dart';
import 'package:waste4change/components/db_connection.dart';
import 'package:waste4change/config/connectivity_watcher.dart';
import 'package:waste4change/config/db_connection.dart';
import 'package:waste4change/config/simple_bloc_observer.dart';

class App {
  final String baseUrl;

  static const DEFAULT_DB = 'default';

  DioClient dioClient;
  ConnectivityWatcher connectivityWatcher;

  Map<String, DBConnection> connections = Map<String, DBConnection>();

  static App _instance;

  App.configure({@required this.baseUrl}) {
    _instance = this;
  }

  factory App() {
    if (_instance == null) {
      throw UnimplementedError('App must be configured first.');
    }

    return _instance;
  }

  void dispose() async {
    connections.forEach((String key, DBConnection value) async {
      (await value.database).close();
    });

    connectivityWatcher.cancelSubscription();
  }

  Future<Null> init() async {
    dioClient = DioClient(baseUrl: baseUrl);
    connectivityWatcher = ConnectivityWatcher();
    connectivityWatcher.init();

    connections = connectionConfig();

    Bloc.observer = SimpleBlocObserver();
  }
}
