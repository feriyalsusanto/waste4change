import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:waste4change/base/entity_base.dart';
import '../app.dart';

abstract class Repository<T extends Entity> {
  String get tableName;

  Repository() {
    registerRelation();
  }

  void registerRelation();

  T toEntity(Map<String, dynamic> map);

  Future<List<T>> query({
    String dbConnection = App.DEFAULT_DB,
    bool distinct,
    List<String> columns,
    String where,
    List<dynamic> whereArgs,
    String groupBy,
    String having,
    String orderBy,
    int limit,
    int offset,
  }) async {
    final Database db = await App().connections[dbConnection].database;
    List<Map<String, dynamic>> rawResults = await db.query(
      tableName,
      distinct: distinct,
      columns: columns,
      where: where,
      whereArgs: whereArgs,
      groupBy: groupBy,
      having: having,
      orderBy: orderBy,
      limit: limit,
      offset: offset,
    );

    List<T> results = List<T>();

    rawResults.forEach((Map<String, dynamic> res) {
      results.add(toEntity(res));
    });

    return results;
  }

  Future<List<dynamic>> batch(
    Function actions(Batch batch), {
    String dbConnection = App.DEFAULT_DB,
    bool exclusive,
    bool noResult,
    bool continueOnError,
  }) async {
    final Database db = await App().connections[dbConnection].database;
    Batch dbBatch = db.batch();

    actions(dbBatch);

    return await dbBatch.commit(
      exclusive: exclusive,
      noResult: noResult,
      continueOnError: continueOnError,
    );
  }

  Future<int> update(
    Entity entity, {
    String dbConnection = App.DEFAULT_DB,
    String primaryKey = 'id',
    ConflictAlgorithm conflictAlgorithm,
  }) async {
    final Database db = await App().connections[dbConnection].database;

    Map<String, dynamic> entityMap = entity.toMap();

    return db.update(
      tableName,
      entityMap,
      where: '$primaryKey = ?',
      whereArgs: [entityMap[primaryKey]],
      conflictAlgorithm: conflictAlgorithm,
    );
  }

  Future<int> insert(Entity entity,
      {String dbConnection = App.DEFAULT_DB,
      String nullColumnHack,
      ConflictAlgorithm conflictAlgorithm}) async {
    final Database db = await App().connections[dbConnection].database;

    return await db.insert(tableName, entity.toMap());
  }

  Future<int> delete(String id,
      {String primaryKey = 'id', String dbConnection = App.DEFAULT_DB}) async {
    final Database db = await App().connections[dbConnection].database;

    return await db
        .delete(tableName, where: '$primaryKey = ?', whereArgs: [id]);
  }

  Future<int> deleteWhere(String where, List<String> whereArgs,
      {String dbConnection = App.DEFAULT_DB}) async {
    final Database db = await App().connections[dbConnection].database;

    return await db.delete(tableName, where: where, whereArgs: whereArgs);
  }

  Future<T> findOne(String id, {String pkField = 'id'}) async {
    List<T> results = await query(where: '$pkField = ?', whereArgs: [id]);
    if (results.length > 0) return results.first;

    return null;
  }

  Future<int> deleteAllRecords({String dbConnection = App.DEFAULT_DB}) async {
    print("delete table $tableName");
    final Database db = await App().connections[dbConnection].database;

    return await db.rawDelete('DELETE FROM $tableName');
  }

  Future<List<T>> rawQuery(String query) async {
    String dbConnection = App.DEFAULT_DB;
    final Database db = await App().connections[dbConnection].database;

    List<Map<String, dynamic>> rawResults = await db.rawQuery(query);

    List<T> results = List<T>();

    rawResults.forEach((Map<String, dynamic> res) {
      results.add(toEntity(res));
    });

    return results;
  }
}
