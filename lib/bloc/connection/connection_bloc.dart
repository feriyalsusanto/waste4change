import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waste4change/bloc/connection/connection.dart';

class ConnectionBloc extends Bloc<ConnectionEvent, ConnectionStatusState> {
  ConnectionBloc() : super(StateConnectionIdle());

  @override
  Stream<ConnectionStatusState> mapEventToState(event) async* {
    if (event is UpdateConnectionStatus) {
      yield StateConnectionUpdated(connected: event.connected);
    }
  }
}
