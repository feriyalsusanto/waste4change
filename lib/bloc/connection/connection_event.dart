import 'package:flutter/material.dart';

abstract class ConnectionEvent {}

class UpdateConnectionStatus extends ConnectionEvent {
  final bool connected;

  UpdateConnectionStatus({@required this.connected});
}
