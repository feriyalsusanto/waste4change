import 'package:flutter/material.dart';

abstract class ConnectionStatusState {}

class StateConnectionIdle extends ConnectionStatusState {}

class StateConnectionUpdated extends ConnectionStatusState {
  final bool connected;

  StateConnectionUpdated({@required this.connected});
}
