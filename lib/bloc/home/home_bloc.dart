import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:waste4change/bloc/home/home.dart';
import 'package:waste4change/models/user_model.dart';
import 'package:waste4change/repository/user_repository.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final UserRepository userRepository;

  HomeBloc({@required this.userRepository})
      : assert(userRepository != null),
        super(StateInitialized());

  @override
  Stream<HomeState> mapEventToState(event) async* {
    if (event is HomeGetData) {
      yield StateHomeLoading();

      List<UserModel> users = List();
      String message = 'Gagal mengambil data';

      var response = await userRepository.getUserList();
      if (response is List<UserModel>) {
        users = response;
        if (users.length == 0)
          message = 'Data Kosong';
        else
          message = 'Berhasil mengambil data';
      } else if (response is Response) {
        message = 'Gagal';
      }

      yield StateHomeLoaded(users: users, message: message);
    }

    if (event is HomeSyncronization) {
      yield StateHomeSyncStart();

      var response = await userRepository.syncUser(event.users);

      yield StateHomeSyncFinished(response: response);
    }
  }
}
