import 'package:flutter/material.dart';
import 'package:waste4change/models/user_model.dart';

abstract class HomeEvent {}

class HomeInit extends HomeEvent {}

class HomeGetData extends HomeEvent {}

class HomeAddData extends HomeEvent {
  final String email;
  final String firstName;
  final String lastName;

  HomeAddData(
      {@required this.email,
      @required this.firstName,
      @required this.lastName});
}

class HomeSyncronization extends HomeEvent {
  final List<UserModel> users;

  HomeSyncronization({@required this.users});
}
