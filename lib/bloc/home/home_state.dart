import 'package:flutter/material.dart';
import 'package:waste4change/models/user_model.dart';

abstract class HomeState {}

class StateInitialized extends HomeState {}

class StateHomeLoading extends HomeState {}

class StateHomeLoaded extends HomeState {
  final List<UserModel> users;
  final String message;

  StateHomeLoaded({@required this.users, @required this.message});
}

class StateHomeSyncStart extends HomeState {}

class StateHomeSyncFinished extends HomeState {
  final dynamic response;

  StateHomeSyncFinished({@required this.response});
}
