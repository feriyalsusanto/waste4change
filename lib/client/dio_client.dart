import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class DioClient {
  Dio dio;

  DioClient({@required String baseUrl}) {
    dio = Dio(BaseOptions(
      headers: {'key': 'Feriyal Susanto'},
      baseUrl: baseUrl,
      connectTimeout: 30 * 1000,
      receiveTimeout: 30 * 1000,
      sendTimeout: 30 * 1000,
      responseType: ResponseType.json,
      receiveDataWhenStatusError: true,
    ));
  }

  Future<Response> get({@required String url}) async {
    try {
      print('request $url');

      Response response = await dio.get(url);

      print('response ${response.data}');

      return response;
    } on DioError catch (e) {
      print('request error ${e.response}');
      if (e.response != null) return e.response;
    }

    return null;
  }

  Future<Response> post(
      {@required String url, @required FormData parameters}) async {
    try {
      print('request $url');
      print('parameters ${parameters.fields}');

      Response response = await dio.post(url, data: parameters);

      print('response ${response.data}');

      return response;
    } on DioError catch (e) {
      print('request error ${e.response}');
      if (e.response != null) return e.response;
    }

    return null;
  }
}
