import 'dart:async';

import 'package:connectivity/connectivity.dart';

class ConnectivityWatcher {
  static ConnectivityWatcher _instance;
  StreamSubscription<ConnectivityResult> connectivitySubscription;
  Connectivity connectivity;

  factory ConnectivityWatcher() {
    if (_instance == null) {
      _instance = ConnectivityWatcher._();
    }

    return _instance;
  }

  ConnectivityWatcher._();

  init() {
    connectivity = Connectivity();
  }

  cancelSubscription() {
    connectivitySubscription.cancel();
  }
}
