import 'package:waste4change/components/db_connection.dart';
import 'package:waste4change/config/migrations_config.dart';

Map<String, DBConnection> connectionConfig() {
  return {
    'default': DBConnection('wasteforchange.db', migrationMainApp),
  };
}
