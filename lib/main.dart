import 'package:flutter/material.dart';
import 'package:waste4change/app.dart';
import 'package:waste4change/main_app.dart';

Future<Null> main() async {
  App.configure(baseUrl: 'http://api.demo.waste4change.com/');

  await App().init();

  runApp(MainApp());
}
