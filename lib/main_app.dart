import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waste4change/app.dart';
import 'package:waste4change/bloc/connection/connection.dart';
import 'package:waste4change/bloc/home/home.dart';
import 'package:waste4change/repository/user_repository.dart';
import 'package:waste4change/screens/splash_screen.dart';
import 'package:waste4change/styles/custom_colors.dart';

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<HomeBloc>(
            create: (BuildContext context) =>
                HomeBloc(userRepository: UserRepository())..add(HomeGetData()),
          ),
          BlocProvider<ConnectionBloc>(
            create: (BuildContext context) => ConnectionBloc(),
          )
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Waste 4 Change',
          theme: ThemeData(
              primarySwatch: kPrimaryColor,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              buttonColor: kLightBlueColor),
          home: SplashScreen(),
        ));
  }

  @override
  void dispose() {
    App().dispose();
    super.dispose();
  }
}
