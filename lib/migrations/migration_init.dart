import 'package:sqflite_common/sqlite_api.dart';
import 'package:waste4change/base/migration_base.dart';

class MigrationInit extends MigrationBase {
  @override
  Future<void> up(Database db) async {
    await db.execute("CREATE TABLE user("
    "id TEXT PRIMARY KEY, "
    "name TEXT, "
    "email TEXT, "
    "date TEXT, "
    "synced BOOLEAN"
    ")");
  }
}
