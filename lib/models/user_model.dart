import 'dart:convert';

import 'package:waste4change/base/entity_base.dart';

class UserModel extends Entity {
  String id;
  String name;
  String email;
  String date;
  bool synced;

  UserModel({
    this.id,
    this.name,
    this.email,
    this.date,
    this.synced,
  });

  UserModel copyWith({
    String id,
    String name,
    String email,
    String date,
    bool synced,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      date: date ?? this.date,
      synced: synced ?? this.synced,
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'name': name,
      'email': email,
      'date': date,
      'synced': synced
    };

    if (id != null) map['id'] = id;

    return map;
  }

  static UserModel fromMap(Map<String, dynamic> map, bool synced) {
    if (map == null) return null;

    print('map $map');

    return UserModel(
        id: map['id'].toString(),
        name: map['name'],
        email: map['email'],
        date: map['date'],
        synced:
            map['synced'] == null ? synced : map['synced'].toString() == '1');
  }

  String toJson() => json.encode(toMap());

  static UserModel fromJson(String source) =>
      fromMap(json.decode(source), true);

  @override
  String toString() {
    return 'UserModel(id: $id, name: $name, email: $email, date: $date, synced: $synced)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is UserModel &&
        o.id == id &&
        o.name == name &&
        o.email == email &&
        o.date == date &&
        o.synced == synced;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        email.hashCode ^
        date.hashCode ^
        synced.hashCode;
  }
}
