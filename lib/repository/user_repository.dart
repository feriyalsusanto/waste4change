import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:waste4change/app.dart';
import 'package:waste4change/base/repository_base.dart';
import 'package:waste4change/models/user_model.dart';
import 'package:waste4change/utils/connectivity_util.dart';
import 'package:waste4change/utils/date_util.dart';

class UserRepository extends Repository {
  @override
  void registerRelation() {}

  @override
  String get tableName => 'user';

  @override
  UserModel toEntity(Map<String, dynamic> map) => UserModel.fromMap(map, true);

  Future<dynamic> getUserList() async {
    try {
      if (await ConnectivityUtil.checkConnectionStatus()) {
        Response response = await App().dioClient.get(url: 'user/all');
        if (response.statusCode == 200) {
          List<UserModel> users = List();

          if (response.data != null)
            users = List.generate(response.data.length, (index) {
              return UserModel.fromMap(response.data[index], true);
            });

          await batchInsert(users);
        }
      }

      List users = await query();
      return users.cast<UserModel>();
    } on DioError catch (e) {
      print('authenticate error ${e.response}');
      if (e.response != null) return e.response;
    }

    return null;
  }

  Future<dynamic> addNewUser(
      {String name,
      String firstName,
      String lastName,
      @required String email}) async {
    try {
      if (await ConnectivityUtil.checkConnectionStatus()) {
        FormData parameters = FormData.fromMap({
          'name':
              (name != null && name.isNotEmpty) ? name : '$firstName $lastName',
          'email': email
        });

        Response response =
            await App().dioClient.post(url: 'user/add', parameters: parameters);

        if (response.statusCode == 200) {
          UserModel userModel = UserModel.fromMap(response.data, true);
          return userModel;
        } else {
          return response;
        }
      } else {
        UserModel userModel = UserModel(
            name: '$firstName $lastName',
            email: email,
            synced: false,
            date: DateUtil.getCurrentDateTime());

        print(userModel.toString());

        await insert(userModel);

        return userModel;
      }
    } on DioError catch (e) {
      print('authenticate error ${e.response}');
      if (e.response != null) return e.response;
    }

    return null;
  }

  Future<dynamic> syncUser(List<UserModel> users) async {
    try {
      for (UserModel user in users) {
        if (!user.synced && await ConnectivityUtil.checkConnectionStatus()) {
          FormData parameters =
              FormData.fromMap({'name': user.name, 'email': user.email});

          Response response = await App()
              .dioClient
              .post(url: 'user/add', parameters: parameters);

          if (response.statusCode == 200) {
            await deleteWhere('email = ?', [user.email]);
          }
        }
      }

      return 'Sinkronisasi data user selesai';
    } on DioError catch (e) {
      print('authenticate error ${e.response}');
      if (e.response != null) return e.response.statusMessage;
    }

    return null;
  }

  Future<int> batchInsert(List<UserModel> users) async {
    List<dynamic> result = await batch((batch) {
      users.forEach((element) {
        batch.rawInsert(
            'INSERT OR REPLACE INTO $tableName(id, name, email, date, synced) VALUES(?,?,?,?,?)',
            [element.id, element.name, element.email, element.date, true]);
      });

      return;
    });

    return result.length;
  }
}
