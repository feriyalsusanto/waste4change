import 'package:flutter/material.dart';

class SplashScreenComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Icon(
          Icons.android,
          size: 64.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
