import 'package:flutter/material.dart';
import 'package:waste4change/screens/home/addnew/components/body.dart';

class AddNewScreen extends StatefulWidget {
  @override
  _AddDataScreenState createState() => _AddDataScreenState();
}

class _AddDataScreenState extends State<AddNewScreen> {
  final GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formState = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      appBar: buildAppBar(),
      body: Body(scaffoldState: scaffoldState, formState: formState),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Text('Input User'),
      centerTitle: true,
    );
  }
}
