import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waste4change/bloc/home/home.dart';
import 'package:waste4change/components/overlay_loading.dart';
import 'package:waste4change/models/user_model.dart';
import 'package:waste4change/repository/user_repository.dart';
import 'package:waste4change/utils/text_util.dart';

class Body extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldState;
  final GlobalKey<FormState> formState;

  final TextEditingController emailController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();

  Body({@required this.scaffoldState, @required this.formState});

  @override
  Widget build(BuildContext context) {
    // BlocBuilder<ConnectionBloc, ConnectionStatusState>(
    //     builder: (context, state) {});
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(16.0),
        color: Colors.white,
        child: Form(
          key: formState,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                'Email',
                style: TextStyle(color: Colors.black54, fontSize: 13.0),
              ),
              SizedBox(height: 8.0),
              TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                    hintText: 'Email',
                    hintStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    floatingLabelBehavior: FloatingLabelBehavior.never),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Email tidak boleh kosong';
                  } else if (!TextUtil.validateEmail(source: value)) {
                    return 'Email tidak valid';
                  }

                  return null;
                },
              ),
              SizedBox(height: 16.0),
              Text(
                'Nama Depan',
                style: TextStyle(color: Colors.black54, fontSize: 13.0),
              ),
              SizedBox(height: 8.0),
              TextFormField(
                controller: firstNameController,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                    hintText: 'Nama Depan',
                    hintStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    floatingLabelBehavior: FloatingLabelBehavior.never),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Nama depan tidak boleh kosong';
                  }

                  return null;
                },
              ),
              SizedBox(height: 16.0),
              Text(
                'Nama Belakang',
                style: TextStyle(color: Colors.black54, fontSize: 13.0),
              ),
              SizedBox(height: 8.0),
              TextFormField(
                controller: lastNameController,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                    hintText: 'Nama Belakang',
                    hintStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    floatingLabelBehavior: FloatingLabelBehavior.never),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Nama Belakang tidak boleh kosong';
                  }

                  return null;
                },
              ),
              SizedBox(height: 16.0),
              RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  onPressed: () => saveButtonListener(context),
                  child: Text(
                    'Simpan',
                    style: TextStyle(color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)))),
            ],
          ),
        ),
      ),
    );
  }

  saveButtonListener(BuildContext context) async {
    formState.currentState.save();

    if (formState.currentState.validate()) {
      OverlayLoading.showLoading(context);

      UserRepository repository = UserRepository();
      var response = await repository.addNewUser(
          email: emailController.text,
          firstName: firstNameController.text,
          lastName: lastNameController.text);

      Navigator.pop(context);
      if (response is UserModel) {
        BlocProvider.of<HomeBloc>(context)..add(HomeGetData());
        Navigator.pop(context);
      } else if (response is Response) {
        scaffoldState.currentState
            .showSnackBar(SnackBar(content: Text(response.statusMessage)));
      }
    }
  }
}
