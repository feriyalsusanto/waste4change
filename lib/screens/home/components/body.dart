import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waste4change/bloc/connection/connection.dart';
import 'package:waste4change/bloc/home/home.dart';
import 'package:waste4change/components/overlay_loading.dart';
import 'package:waste4change/models/user_model.dart';
import 'package:waste4change/screens/home/addnew/add_new_screen.dart';
import 'package:waste4change/screens/home/components/list_item_body.dart';
import 'package:waste4change/styles/custom_colors.dart';

class Body extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldState;
  final List<UserModel> users = List();

  Body({@required this.scaffoldState});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectionBloc, ConnectionStatusState>(
        builder: (context, state) {
      bool connected = true;
      if (state is StateConnectionUpdated) {
        connected = state.connected;
      }

      return BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
        return BlocListener<HomeBloc, HomeState>(
            listener: (context, state) {
              if (state is StateHomeLoading || state is StateHomeSyncStart) {
                OverlayLoading.showLoading(context);
              } else if (state is StateHomeLoaded ||
                  state is StateHomeSyncFinished) {
                Navigator.pop(context);
                if (state is StateHomeSyncFinished) {
                  scaffoldState.currentState.showSnackBar(SnackBar(
                    content: Text(state.response),
                    duration: Duration(seconds: 1),
                  ));
                  BlocProvider.of<HomeBloc>(context)..add(HomeGetData());
                }
              }
            },
            child: buildBodyWidget(context, state, connected));
      });
    });
  }

  Container buildBodyWidget(
      BuildContext context, HomeState state, bool connected) {
    if (state is StateHomeLoaded) {
      users.clear();
      users.addAll(state.users);
    }

    return Container(
      color: kBackgroundColor,
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              onPressed: () => addButtonListener(context),
              child: Text(
                'Tambah',
                style: TextStyle(color: Colors.white),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)))),
          SizedBox(height: 8.0),
          Expanded(
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    UserModel user = users[index];

                    return ListItemBody(user: user);
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(
                      height: 8.0,
                    );
                  },
                  itemCount: users.length)),
          SizedBox(height: 8.0),
          buildSyncButtonWidget(context, state, users, connected),
          // buildConnectionStatus(connected)
        ],
      ),
    );
  }

  buildSyncButtonWidget(BuildContext context, HomeState state,
      List<UserModel> users, bool connected) {
    bool show = false;
    if (state is StateHomeLoaded) {
      state.users.forEach((element) {
        if (!element.synced) show = true;
      });
    }

    return show
        ? FlatButton(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            onPressed:
                connected ? () => syncButtonListener(context, users) : null,
            child: Text('Singkronisasi',
                style: TextStyle(
                    color: connected ? kLightBlueColor : Colors.grey)),
            color: Colors.transparent,
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: connected ? kLightBlueColor : Colors.grey),
                borderRadius: BorderRadius.all(Radius.circular(8.0))))
        : Container();
  }

  buildConnectionStatus(bool connected) {
    if (connected) return Container();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.brightness_1,
            color: Colors.red,
            size: 12.0,
          ),
          SizedBox(width: 4.0),
          Text(
            'Tidak tersambung dengan internet',
            style: TextStyle(color: Colors.red, fontSize: 12.0),
          )
        ],
      ),
      decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)))),
    );
  }

  addButtonListener(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return AddNewScreen();
    }));
  }

  syncButtonListener(BuildContext context, List<UserModel> users) {
    BlocProvider.of<HomeBloc>(context)..add(HomeSyncronization(users: users));
  }
}
