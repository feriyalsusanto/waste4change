import 'package:flutter/material.dart';
import 'package:waste4change/models/user_model.dart';

class ListItemBody extends StatelessWidget {
  final UserModel user;

  ListItemBody({@required this.user});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: user.synced ? Colors.white : Colors.grey[300],
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Text(
                  user.name,
                  style: TextStyle(color: Colors.black, fontSize: 14.0),
                ),
                buildUnsyncWidget()
              ],
            ),
            SizedBox(
              height: 8.0,
            ),
            Text(
              user.email,
              style: TextStyle(color: Colors.grey, fontSize: 12.0),
            )
          ],
        ),
      ),
    );
  }

  buildUnsyncWidget() {
    return user.synced
        ? Container()
        : Expanded(
            child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                Icons.query_builder,
                size: 12.0,
                color: Colors.grey,
              ),
              SizedBox(width: 4.0),
              Text(
                'Menunggu Koneksi',
                style: TextStyle(color: Colors.grey, fontSize: 12.0),
                textAlign: TextAlign.right,
              )
            ],
          ));
  }
}
