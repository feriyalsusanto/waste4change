import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waste4change/app.dart';
import 'package:waste4change/bloc/connection/connection.dart';
import 'package:waste4change/screens/home/components/body.dart';
import 'package:waste4change/utils/connectivity_util.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    startConnectionWatcher();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      appBar: buildAppBar(),
      body: Body(scaffoldState: scaffoldState),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Text('Data User'),
      centerTitle: true,
      elevation: 0,
    );
  }

  startConnectionWatcher() {
    Connectivity _connectivity = Connectivity();
    App().connectivityWatcher.connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  _updateConnectionStatus(ConnectivityResult connectivityResult) async {
    bool connected =
        await ConnectivityUtil.connectionStatus(connectivityResult);
    BlocProvider.of<ConnectionBloc>(context)
      ..add(UpdateConnectionStatus(connected: connected));
  }
}
