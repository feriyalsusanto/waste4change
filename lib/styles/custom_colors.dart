import 'package:flutter/material.dart';

const MaterialColor kPrimaryColor = MaterialColor(_kBasePrimaryColor, <int, Color>{
  50: Color(0xFFE2E6EC),
  100: Color(0xFFB6C2CF),
  200: Color(0xFF8699AF),
  300: Color(0xFF55708F),
  400: Color(0xFF305177),
  500: Color(_kBasePrimaryColor),
  600: Color(0xFF0A2D57),
  700: Color(0xFF08264D),
  800: Color(0xFF061F43),
  900: Color(0xFF031332),
});
const int _kBasePrimaryColor = 0xFF0C325F;

const Color kBackgroundColor = Color(0xFFF9F8FD);
const Color kLightBlueColor = Color(0xFF30AEE4);