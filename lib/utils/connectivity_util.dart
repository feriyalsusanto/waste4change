import 'package:connectivity/connectivity.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:waste4change/app.dart';

class ConnectivityUtil {
  static Future<bool> checkConnectionStatus() async {
    ConnectivityResult connectivityResult =
        await App().connectivityWatcher.connectivity.checkConnectivity();
    return connectionStatus(connectivityResult);
  }

  static Future<bool> connectionStatus(
      ConnectivityResult connectivityResult) async {
    if (connectivityResult == ConnectivityResult.mobile) {
      if (await DataConnectionChecker().hasConnection) {
        return true;
      }
    } else if (connectivityResult == ConnectivityResult.wifi) {
      if (await DataConnectionChecker().hasConnection) {
        return true;
      }
    }

    return false;
  }
}
