import 'package:date_format/date_format.dart';

class DateUtil {
  static String getCurrentDateTime({List<String> formats}) {
    return formatDate(DateTime.now(),
        formats ?? [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]);
  }
}
